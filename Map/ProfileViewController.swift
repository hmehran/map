//
//  ProfileViewController.swift
//  Map
//
//  Created by Hamoon Mehran on 2/27/18.
//  Copyright © 2018 Hamoon Mehran. All rights reserved.
//

import Foundation
import UIKit

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    
    
    @IBOutlet var tableView: UITableView!
    
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUp()
        setUpHeaderView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUp()
    {
        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        var rightButton: UIBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(rightbutton(_:)))
        //image = UIImage.init(named: "settings")
        rightButton.image = UIImage.init(named: "settings")

        self.navigationItem.rightBarButtonItem = rightButton
        
        //self.navigationItem.backBarButtonItem?.image = UIImage.init(named: "xButton")
        self.navigationItem.leftBarButtonItem?.image = UIImage.init(named: "xButton")
        
        let segment: UISegmentedControl = UISegmentedControl(items: ["Profile", "Notifications"])
        segment.sizeToFit()
        segment.tintColor = UIColor(red:0.99, green:0.00, blue:0.25, alpha:1.00)
        segment.selectedSegmentIndex = 0;
        //segment.addTarget(self, action: "action:", forControlEvents: .ValueChanged)
        

        
        self.navigationItem.titleView = segment
    }
    
    func setUpHeaderView(){
        var headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/3))
        
        
        var label = UILabel.init(frame: CGRect(x: headerView.bounds.width/2 - 75, y: headerView.bounds.height/2 - 100, width: 150, height: 100))
        label.numberOfLines = 0
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        let attString = NSMutableAttributedString(string: "You \n @You | 000020    ")
        
        attString.addAttribute(.font, value: UIFont.init(name: "BanglaSangamMN-Bold", size: 15)!, range: NSRange.init(location: 0, length: 3))
        
        label.attributedText = attString
       
        
        headerView.addSubview(label)
        
        var addButton = UIButton.init(frame: CGRect(x: headerView.bounds.width/2 - 50, y: headerView.bounds.height/2 , width: 100, height: 100))
        addButton.layer.masksToBounds = true
        addButton.layer.cornerRadius = addButton.bounds.width/2
        
        
        addButton.layer.borderWidth = 1.0
        addButton.layer.borderColor = UIColor.black.cgColor
        
        addButton.setTitle("Add Photo", for: .normal)
        addButton.setTitleColor(UIColor(red:0.99, green:0.36, blue:0.99, alpha:1.00), for: .normal)
        
        
        headerView.addSubview(addButton)
        
        self.tableView.tableHeaderView = headerView
    }
    
    @objc func rightbutton(_ sender: UIButton!) {
        print("right button Pressed")
    }
    
    //MARK: Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        
        
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Add Friends"
            cell.tintColor = UIColor(red:0.99, green:0.36, blue:0.99, alpha:1.00)
            cell.imageView?.image = UIImage(named: "addfriends")
            
        case 1:
            cell.textLabel?.text = "Favorite Places"
            cell.tintColor = UIColor(red:0.99, green:0.36, blue:0.99, alpha:1.00)
            cell.imageView?.image = UIImage(named: "location")
        case 2:
            cell.textLabel?.text = "Invisible"
            cell.imageView?.image = UIImage(named: "invisible")
            
            let switchView = UISwitch.init(frame: CGRect.init(x: 0, y: 0, width: 25, height: 25))
            cell.accessoryView = switchView
        default:
            cell.textLabel?.text = "0"
        }
        
        return cell
    }
    
}

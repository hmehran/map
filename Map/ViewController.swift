//
//  ViewController.swift
//  Map
//
//  Created by Hamoon Mehran on 2/23/18.
//  Copyright © 2018 Hamoon Mehran. All rights reserved.
//

import UIKit
import Mapbox

class MyCustomPointAnnotation: MGLPointAnnotation
{
    var isUser:Bool = false
}

class ViewController: UIViewController, MGLMapViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{

    
    
    var mapView: MGLMapView!
    var you:MyCustomPointAnnotation!
    var friend:MyCustomPointAnnotation!
    //var you: MGLPointAnnotation!
    //var friend: MGLPointAnnotation!
    var collectionView:UICollectionView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupMap()
        addButtons()
        addMarkers()
        setUpCollectionView()
        
    
        
        //centers the map on user
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setupMap()
    {
        let url = URL(string: "mapbox://styles/mapbox/streets-v10")
        mapView = MGLMapView(frame: view.bounds, styleURL: url)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        mapView.setCenter(CLLocationCoordinate2D(latitude: 33.779304, longitude: -118.420019), zoomLevel: 20, animated: false)
        
        
        let bounds = MGLCoordinateBounds(
            sw: CLLocationCoordinate2D(latitude: 33.767, longitude: -118.40),
            ne: CLLocationCoordinate2D(latitude: 33.77, longitude: -118.43))
        mapView.setVisibleCoordinateBounds(bounds, animated: false)
 
        
        mapView.delegate = self
        
        //delete later
        //mapView.userTrackingMode = .followWithHeading
        mapView.showsUserHeadingIndicator = true
        
        mapView.logoView.isHidden = true
        mapView.attributionButton.isHidden = true
        //map_view.attributionButton.frame = CGRect(x: view.bounds.width - view.bounds.width/10, y: view.bounds.height/12.5, width: 25, height: 25)
        //map_view.attributionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
        mapView.attributionButton.contentVerticalAlignment = UIControlContentVerticalAlignment.top
        
        
        view.addSubview(mapView)
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addMarkers()
    {
        //you = MGLPointAnnotation()
        you = MyCustomPointAnnotation()
        you.isUser = true
        
        you.coordinate = CLLocationCoordinate2D(latitude: 33.779, longitude: -118.418)
        you.title = "You"
        you.subtitle = "This is your location"
        
        
    
        //friend
        
        //friend = MGLPointAnnotation()
        friend = MyCustomPointAnnotation()
        friend.isUser = false
        friend.coordinate = CLLocationCoordinate2D(latitude: 33.78, longitude: -118.422)
        friend.title = "Friend"
        friend.subtitle = "This is your friend's location"
        
        mapView.addAnnotations([you, friend])
        
    }
    
    
    
    // MARK: buttons
    
    func addButtons()
    {
        let profileBttn = UIButton (type: UIButtonType.custom) as UIButton
        
        
        profileBttn.frame = CGRect(x: view.bounds.width/10, y: view.bounds.height/12.5, width: 25, height: 25)
       
        profileBttn.setImage(UIImage(named:"you"), for: .normal)
        
        profileBttn.setTitleColor(UIColor.black, for: UIControlState.normal)
        profileBttn.addTarget(self, action: #selector(ViewController.profileButtonPressed(_:)), for: UIControlEvents.touchUpInside )
        profileBttn.layer.masksToBounds = true
        profileBttn.layer.cornerRadius = profileBttn.frame.width/2
       
        
        
        self.view.addSubview(profileBttn)
        //self.view.bringSubview(toFront: profileBttn)
        
        
        let navButton = UIButton (type: UIButtonType.system) as UIButton
        navButton.frame =  CGRect(x: self.view.bounds.width/10, y: view.bounds.height - view.bounds.height/4.5, width: 25, height: 25)
        navButton.addTarget(self, action: #selector(ViewController.navButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        navButton.setImage(UIImage(named:"navigation"), for: UIControlState.normal)
        navButton.tintColor = UIColor.black
           self.view.addSubview(navButton)
            
        let friendButton = UIButton (type: UIButtonType.custom) as UIButton
        
        friendButton.frame = CGRect (x: view.bounds.width  - view.bounds.width/10, y: view.bounds.height - view.bounds.height/4.5, width: 25, height: 25)
        friendButton.setImage(UIImage(named:"emoji2"), for: UIControlState.normal)
        friendButton.addTarget(self, action: #selector(ViewController.friendButtonPressed(_:)) , for: UIControlEvents.touchUpInside)
        friendButton.layer.masksToBounds = true
        friendButton.layer.cornerRadius = friendButton.frame.width / 2
        
        view.addSubview(friendButton)
        
        let happenButton = UIButton (type: .custom) as UIButton
        
       happenButton.frame = CGRect (x: view.bounds.width/2 - 100, y: view.bounds.height/12.5, width: 100, height: 25)
        happenButton.setTitle("Make Something Happen", for: .normal)
        happenButton.sizeToFit()
        happenButton.setTitleColor(UIColor(red:0.99, green:0.36, blue:0.99, alpha:1.00), for: .normal)
       
        happenButton.addTarget(self, action: #selector(ViewController.happenButtonPressed(_ :)), for: .touchUpInside)
        
        //happenButton.layer.masksToBounds = true
        happenButton.clipsToBounds = true
        happenButton.layer.cornerRadius = happenButton.frame.height / 2
        happenButton.backgroundColor = UIColor.white
        
        let shadowPath = UIBezierPath(rect: happenButton.bounds)
        happenButton.layer.masksToBounds = false
        happenButton.layer.shadowColor = UIColor.init(red: 0.99, green: 0.36, blue: 0.99, alpha: 1.00).cgColor
        
        happenButton.layer.shadowOffset = CGSize(width: 0, height: 0.1)
        happenButton.layer.shadowOpacity = 0.05
        happenButton.layer.shadowPath = shadowPath.cgPath
        
        view.addSubview(happenButton)
        
    }
    
    @objc func profileButtonPressed(_ sender:UIButton! ) {
        print("profile Button Clicked")
        performSegue(withIdentifier: "segueToProfileViewController", sender: self)
    }
   
    @objc func navButtonPressed(_ sender:UIButton! ) {
        print("nav button clicked")
        mapView(mapView, didSelect:self.you)
        let indexPath:IndexPath! = self.collectionView.indexPathsForSelectedItems?.last ?? IndexPath(item: 1, section: 0)
        self.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.centeredHorizontally)
    }
    
    @objc func friendButtonPressed(_ sender:UIButton!) {
        print("friend button pressed")
        
    }
    
    @objc func happenButtonPressed(_ sender:UIButton!) {
        print("happen button pressed")
        performSegue(withIdentifier: "segueToHappenViewController", sender: self)
    }
    
    //MARK: Map Box Methods
    

    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage?
    {
        
        var imageString: String!
        if let castAnnotation = annotation as? MyCustomPointAnnotation
        {
            if (castAnnotation.isUser)
            {
                //return nil;
                imageString = "you_button"
                
            }else{
                imageString = "friend"
            }
        }
        
        let image = UIImage(named:imageString)!
        
        //image = image.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: 0, bottom: image.size.height/100, right: 0))
        
        
        let annotationImage = MGLAnnotationImage(image: image, reuseIdentifier:imageString)
        
        
        
        
        /*
        var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: "friend")

        if (annotationImage == nil)
        {
            
            var image = UIImage(named:imageString)!
            
            image = image.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: 0, bottom: image.size.height/100, right: 0))
            
            
           
            annotationImage = MGLAnnotationImage(image: image, reuseIdentifier:"friend")
           
            
        
        
        }*/
        
        return annotationImage
        
    
    }

    
    /*
    // Use the default marker. See also: our view annotation or custom marker examples.
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        if let castAnnotation = annotation as? MyCustomPointAnnotation {
            if(castAnnotation.isUser){
                return nil
            }
        }
        
    }*/
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool
    {
        return true
    }
    
    //map view did click
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation)
    {
        
        var imageString: String!
        if let castAnnotation = annotation as? MyCustomPointAnnotation
        {
            var item:NSInteger!
            
            if (castAnnotation.isUser)
            {
                item = 1
                print("is user")
                
            }else{
                item = 2
            }
            
            let indexPath:IndexPath! = self.collectionView.indexPathsForSelectedItems?.last ?? IndexPath(item: item, section: 0)
            self.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.centeredHorizontally)
        }
        
        let camera = MGLMapCamera(lookingAtCenter: annotation.coordinate, fromDistance: 4000, pitch: 0, heading: 0)
        mapView.setCamera(camera, animated: true)
    }

    //MARK: Collection View
    func setUpCollectionView(){
    
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        let frame = CGRect(x: view.bounds.width/15, y: view.bounds.height, width: view.bounds.width - view.bounds.width/15, height: -view.bounds.height/5)
        collectionView = UICollectionView(frame: frame, collectionViewLayout: flowLayout)
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.clear
        
        self.view.addSubview(collectionView)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath)
        
        cell.backgroundColor = UIColor.white
        //cell.layer.masksToBounds = true
        cell.clipsToBounds = true
        cell.layer.cornerRadius = cell.frame.height/2
        let shadowPath = UIBezierPath(rect: cell.bounds)
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.init(red: 0.99, green: 0.36, blue: 0.99, alpha: 1.00).cgColor
        
        cell.layer.shadowOffset = CGSize(width: 0, height: 0.1)
        cell.layer.shadowOpacity = 0.05
        cell.layer.shadowPath = shadowPath.cgPath
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height))
        label.textColor = .black
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont.init(name: "BanglaSangamMN", size: 15)
        
        cell.addSubview(label)
        
        
        switch indexPath.row {
        case 0:
            label.text = "Mappen is better with close friends"
            label.font = UIFont(name: "BanglaSangamMN-Bold", size: 19)
        case 1:
            let attString = NSMutableAttributedString(string: "You \n Palos Verdes, CA Just now    ")
            attString.addAttribute(.font, value: UIFont.init(name: "BanglaSangamMN-Bold", size: 15)!, range: NSRange.init(location: 0, length: 3))
            
            label.numberOfLines = 0
            label.textAlignment = .center
            label.attributedText = attString
            
        case 2:
           
            let attributedString = NSMutableAttributedString.init(string: "Friend \n Palos Verdes, CA Just now   ")
            attributedString.addAttribute(.font, value: UIFont.init(name: "BanglaSangamMN-Bold", size: 19)!, range: NSRange.init(location: 0, length: 6))
            
            label.attributedText = attributedString
            label.numberOfLines = 0
            label.textAlignment = .center
            
        default:
            label.text = ""
            
            
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: view.bounds.width - view.bounds.width/10, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var item:Int = 0
        
        switch indexPath.row {
        case 0:
            print("1st")
            item = 0
        case 1:
            print("2nd")
            item = 1
            mapView(self.mapView, didSelect: you)
        case 2:
             print("3rd")
            item = 2
             mapView(self.mapView, didSelect:friend)
        default:
            print("nothing")
        }
        
        let indexPath = self.collectionView.indexPathsForSelectedItems?.last ?? IndexPath(item: item, section: 0)
        self.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.centeredHorizontally)
    }
    
    /*
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }*/
    
}

